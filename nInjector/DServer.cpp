#include "stdafx.h"
#include "DServer.h"

using namespace std;

void DServer::SetLicense(int license)
{
	License = license;
}

void DServer::SetStatus(string status)
{
	Status = status;
}

void DServer::SetVersion(string version)
{
	Version = version;
}

void DServer::SetActive(bool active)
{
	Active = active;
}

void DServer::SetSafe(bool safe)
{
	Safe = safe;
}

int DServer::GetLicense()
{
	return License;
}

bool DServer::GetActive()
{
	return Active;
}

bool DServer::GetSafe()
{
	return Safe;
}

string DServer::GetStatus()
{
	return Status;
}

string DServer::GetVersion()
{
	return Version;
}

