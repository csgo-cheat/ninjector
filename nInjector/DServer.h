#pragma once

using namespace std;

class DServer
{
	int License;
	string Status;
	string Version;
	bool Active;
	bool Safe;


public:
	void SetLicense(int license);
	void SetStatus(string status);
	void SetVersion(string version);
	void SetActive(bool active);
	void SetSafe(bool safe);
	int GetLicense();
	bool GetActive();
	bool GetSafe();
	string GetStatus();
	string GetVersion();
};

