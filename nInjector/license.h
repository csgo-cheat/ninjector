#pragma once

#include "base64.h"

namespace license {
	std::string generateUniqueID();
	int getVolumeHash();
	int getCpuHash();
	const char * getMachineName();

};

