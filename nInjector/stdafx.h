// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
// Windows Header Files:
#include <windows.h>

// C RunTime Header Files
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

#include <intrin.h>
#include <string>
#include <string>
#include <atlstr.h>

#include <curl\curl.h>
#include <json\json.h>

#include <cstdint>
#include <iostream>

#include <Commdlg.h>
#include <sstream>

#include <tlhelp32.h>


// TODO: reference additional headers your program requires here
