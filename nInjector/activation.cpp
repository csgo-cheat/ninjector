#include "stdafx.h"
#include "activation.h"

using namespace std;

namespace activation {

	namespace
	{

		size_t callback(
			const char* in,
			size_t size,
			size_t num,
			char* out)
		{
			string data(in, (std::size_t) size * num);
			*((stringstream*) out) << data;
			return size * num;
		}
	}

	DServer DOHtppCall() {

		string url = "http://vacbax.ir/api/v1/license/";
		url.append(license::generateUniqueID());

		CURL* curl = curl_easy_init();

		// Set remote URL.
		curl_easy_setopt(curl, CURLOPT_URL, url.c_str());

		// Don't bother trying IPv6, which would increase DNS resolution time.
		curl_easy_setopt(curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);

		// Don't wait forever, time out after 10 seconds.
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

		// Follow HTTP redirects if necessary.
		curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

		// Response information.
		int httpCode(0);
		stringstream httpData;

		// Hook up data handling function.
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, callback);

		// Hook up data container (will be passed as the last parameter to the
		// callback handling function).  Can be any pointer type, since it will
		// internally be passed as a void pointer.
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &httpData);

		// Run our HTTP GET command, capture the HTTP response code, and clean up.
		curl_easy_perform(curl);
		curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &httpCode);
		curl_easy_cleanup(curl);

		
		if (httpCode == 200)
		{
			// Response looks good - done using Curl now.  Try to parse the results
			// and print them out.
			Json::Value jsonData;
			Json::CharReaderBuilder jsonReader;
			string errs;
			

			if (Json::parseFromStream(jsonReader, httpData, &jsonData, &errs))
			{

				const string version(jsonData["version"].asString());
				const string status(jsonData["status"].asString());
				const int license(jsonData["license"].asInt());
				const bool active(jsonData["active"].asBool());
				const bool safe(jsonData["safe"].asBool());

				DServer server;
				server.SetLicense(license);
				server.SetStatus(status);
				server.SetVersion(version);
				server.SetActive(active);
				server.SetSafe(safe);

				return server;
			}

			

		}


		MessageBox(0, "Please Check Your Internet Connection!\nError Code: 320", "Activation Failed", MB_OK | MB_SYSTEMMODAL);

		

		DServer server;
		server.SetLicense(0);
		server.SetStatus("Unknown");
		server.SetVersion("");
		server.SetActive(false);
		server.SetSafe(true);

		return server;


	}

}
