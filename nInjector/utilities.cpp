#include "stdafx.h"
#include "utilities.h"

using namespace std;

namespace utilities {


	LPCSTR ConvertString(string data) {
		return data.c_str();
	}

	void copyToClipboard(HWND hWnd, const std::string &s) {
		OpenClipboard(hWnd);
		EmptyClipboard();
		HGLOBAL hg = GlobalAlloc(GMEM_MOVEABLE, s.size() + 1);
		if (!hg) {
			CloseClipboard();
			return;
		}
		memcpy(GlobalLock(hg), s.c_str(), s.size() + 1);
		GlobalUnlock(hg);
		SetClipboardData(CF_TEXT, hg);
		CloseClipboard();
		GlobalFree(hg);
	}

	DWORD GetProcessByName(PCSTR name)
	{
		DWORD pid = 0;

		// Create toolhelp snapshot.
		HANDLE snapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
		PROCESSENTRY32 process;
		ZeroMemory(&process, sizeof(process));
		process.dwSize = sizeof(process);

		// Walkthrough all processes.
		if (Process32First(snapshot, &process)) {
			do {
				// Compare process.szExeFile based on format of name, i.e., trim file path
				// trim .exe if necessary, etc.
				if (string(process.szExeFile) == string(name)) {
					pid = process.th32ProcessID;
					break;
				}
			} while (Process32Next(snapshot, &process));
		}

		CloseHandle(snapshot);

		return pid;
	}

	bool Inject(DWORD pId, char *dllName)
	{
		HANDLE h = OpenProcess(PROCESS_ALL_ACCESS, false, pId);
		if (h) {
			auto LoadLibAddr = (LPVOID)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA");
			LPVOID dereercomp = VirtualAllocEx(h, nullptr, strlen(dllName), MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
			WriteProcessMemory(h, dereercomp, dllName, strlen(dllName), nullptr);
			HANDLE asdc = CreateRemoteThread(h, nullptr, NULL, (LPTHREAD_START_ROUTINE)LoadLibAddr, dereercomp, 0,
				nullptr);
			WaitForSingleObject(asdc, INFINITE);
			VirtualFreeEx(h, dereercomp, strlen(dllName), MEM_RELEASE);
			CloseHandle(asdc);
			CloseHandle(h);
			return true;

		}
		return false;
	}

}