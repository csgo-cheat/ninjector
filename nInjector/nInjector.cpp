// nInjector.cpp : Defines the entry point for the application.
//

/*
https://docs.lenux.org/libcurl-mit-visual-studio-2017
nmake /f Makefile.vc mode=static MACHINE=x86 vc=15 ENABLE_WINSSL=no GEN_PDB=yes DEBUG=no

vspkg install jsoncpp
.vcxproj file add
<VcpkgConfiguration Condition="'$(Configuration)' == 'Release'">Release</VcpkgConfiguration>
*/

#include "stdafx.h"
#include "nInjector.h"

#define MAX_LOADSTRING 100

#define BUTTON_LUANCH 1001
#define BUTTON_BUY 1002
#define BUTTON_ACTIVATION 1003
#define BUTTON_APARAT 1004
#define BUTTON_TELEGRAM 10065

#define VERSION_LABEL 2001
#define STATUS_LABEL 2002
#define LICENSE_LABEL 2003

#define VERSION_VALUE 3001
#define STATUS_VALUE 3002
#define LICENSE_VALUE 3003

#define APP_VERSION 1

#define WHITE RGB(255, 255, 255)
#define RED RGB(255, 23, 68)
#define BLUE RGB(41, 121, 255)
#define GREEN RGB(0, 230, 118)
#define YELLOW RGB(255, 234, 0)

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

DServer Sdata;
static HBITMAP hBitmap;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
	Sdata = activation::DOHtppCall();
	/*
	Sdata.SetLicense(10);
	Sdata.SetStatus("Unknown");
	Sdata.SetVersion("");
	Sdata.SetActive(true);
	Sdata.SetSafe(true);
	*/
	hBitmap = LoadBitmap(GetModuleHandle(NULL),
		MAKEINTRESOURCE(IDB_BITMAP1));

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_NINJECTOR, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDI_ICON1));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON1));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDI_ICON1);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
      CW_USEDEFAULT, 0, 600, 400, nullptr, nullptr, hInstance, nullptr);


   if (!hWnd)
   {
      return FALSE;
   }

   SetMenu(hWnd, NULL);
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_SIZE:
		InvalidateRect(hWnd, NULL, FALSE);
		break;
	case WM_CREATE:
	{
		HWND btn_start =  CreateWindow(TEXT("button"), TEXT("Start"),
			WS_VISIBLE | WS_CHILD,
			20, 20, 80, 25,
			hWnd, (HMENU) BUTTON_LUANCH, NULL, NULL);

		HWND btn_buy = CreateWindow(TEXT("button"), TEXT("Buy"),
			WS_VISIBLE | WS_CHILD,
			105, 20, 80, 25,
			hWnd, (HMENU) BUTTON_BUY, NULL, NULL);

		HWND btn_active = CreateWindow(TEXT("button"), TEXT("Activation Key"),
			WS_VISIBLE | WS_CHILD,
			20, 50, 165, 25,
			hWnd, (HMENU)BUTTON_ACTIVATION, NULL, NULL);

		HWND lbl_version = CreateWindow(TEXT("STATIC"), TEXT("Version: ") ,
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			20, 80, 60, 25,
			hWnd, (HMENU) VERSION_LABEL, NULL, NULL);

		//string Version = Sdata.GetVersion();

		HWND lbl_version_value = CreateWindow(TEXT("STATIC"), to_string(APP_VERSION).c_str(),
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			80, 80, 300, 25,
			hWnd, (HMENU)VERSION_VALUE, NULL, NULL);

		HWND lbl_status = CreateWindow(TEXT("STATIC"), TEXT("Status: "),
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			20, 100, 60, 25,
			hWnd, (HMENU) STATUS_LABEL, NULL, NULL);

		HWND lbl_status_value = CreateWindow(TEXT("STATIC"), Sdata.GetStatus().c_str(),
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			80, 100, 300, 25,
			hWnd, (HMENU)STATUS_VALUE, NULL, NULL);

		HWND lbl_license = CreateWindow(TEXT("STATIC"), TEXT("License: "),
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			20, 120, 60, 25,
			hWnd, (HMENU) LICENSE_LABEL, NULL, NULL);

		string daysLeft = " Days Left";

		HWND lbl_license_value = CreateWindow(TEXT("STATIC"), to_string(Sdata.GetLicense()).append(daysLeft).c_str(),
			WS_VISIBLE | WS_CHILD | SS_SIMPLE,
			80, 120, 300, 25,
			hWnd, (HMENU)LICENSE_VALUE, NULL, NULL);

		HWND btn_aparat = CreateWindow(TEXT("button"), TEXT("Telegram"),
			WS_VISIBLE | WS_CHILD,
			20, 320, 80, 25,
			hWnd, (HMENU)BUTTON_TELEGRAM, NULL, NULL);

		HWND btn_telegram = CreateWindow(TEXT("button"), TEXT("Aparat"),
			WS_VISIBLE | WS_CHILD,
			105, 320, 80, 25,
			hWnd, (HMENU)BUTTON_APARAT, NULL, NULL);

		HFONT hFont = CreateFont(
			18, 0, 0, 0, FW_MEDIUM, FALSE, FALSE, FALSE, ANSI_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
			DEFAULT_PITCH | FF_DONTCARE, "Roboto");

		SendMessage(btn_start, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(btn_buy, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(btn_active, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(btn_telegram, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(btn_aparat, WM_SETFONT, (WPARAM)hFont, TRUE);
		
		SendMessage(lbl_status, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(lbl_status_value, WM_SETFONT, (WPARAM)hFont, TRUE);

		SendMessage(lbl_license, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(lbl_license_value, WM_SETFONT, (WPARAM)hFont, TRUE);

		SendMessage(lbl_version, WM_SETFONT, (WPARAM)hFont, TRUE);
		SendMessage(lbl_version_value, WM_SETFONT, (WPARAM)hFont, TRUE);
	}
	break;
	case WM_CTLCOLORSTATIC:

		if (GetDlgItem(hWnd, VERSION_LABEL) == (HWND)lParam)
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, WHITE);
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

		if (GetDlgItem(hWnd, STATUS_LABEL) == (HWND)lParam)
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, WHITE);
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

		if (GetDlgItem(hWnd, LICENSE_LABEL) == (HWND)lParam)
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, WHITE);
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

		if (GetDlgItem(hWnd, LICENSE_VALUE) == (HWND)lParam)
		{
			const int days = Sdata.GetLicense();
			
			if (days <= 5) {
				SetTextColor((HDC)wParam, RED);
			}		
			else {
				SetTextColor((HDC)wParam, GREEN);
			}

			SetBkMode((HDC)wParam, TRANSPARENT);
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

		if (GetDlgItem(hWnd, STATUS_VALUE) == (HWND)lParam)
		{
			const string status = Sdata.GetStatus();

			if (status == "Undetected") {
				SetTextColor((HDC)wParam, GREEN);
			}
			if (status == "Detected")
			{
				SetTextColor((HDC)wParam, RED);
			}
			if (status == "Unknown")
			{
				SetTextColor((HDC)wParam, YELLOW);
			}

			SetBkMode((HDC)wParam, TRANSPARENT);
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

		if (GetDlgItem(hWnd, VERSION_VALUE) == (HWND)lParam)
		{
			SetBkMode((HDC)wParam, TRANSPARENT);
			SetTextColor((HDC)wParam, RGB(41, 121, 255));
			return (BOOL)GetStockObject(HOLLOW_BRUSH);
		}

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            switch (wmId)
            {
			case BUTTON_BUY:
				system("start http://vacbax.ir", SW_HIDE);
				break;

			case BUTTON_LUANCH:
			{
				if (!Sdata.GetActive()) {
					MessageBox(hWnd, "Your License Is not Valid, Try Buy Button!", "Info", NULL);
					break;
				}

				if (!Sdata.GetSafe()) {
					MessageBox(hWnd, "It is not safe to run this cheat, you may get VAC.\nPlease try again later.", "Warning", NULL);
					break;
				}

				DoInjection(hWnd);
			}
				break;

			case BUTTON_ACTIVATION:
				{
					string licenseCode = license::generateUniqueID();
					utilities::copyToClipboard(hWnd, licenseCode);
					MessageBox(hWnd, "Your PC Code Copied to Clipboard.", "Info", NULL);
					break;
				}
			case BUTTON_APARAT:
				system("start https://www.aparat.com/vacbax", SW_HIDE);
				break;
			case BUTTON_TELEGRAM:
				system("start https://t.me/VACBAX");
				break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
		HDC hdcInst, hdcBitmap;
		PAINTSTRUCT ps;
		BITMAP bp;
		RECT r;

		hdcInst = BeginPaint(hWnd, &ps);

		// Create a memory device compatible with the above DC variable

		hdcBitmap = CreateCompatibleDC(hdcInst);

		// Select the new bitmap

		SelectObject(hdcBitmap, hBitmap);

		GetObject(hBitmap, sizeof(bp), &bp);

		// Get client coordinates for the StretchBlt() function

		GetClientRect(hWnd, &r);

		// stretch bitmap across client area

		StretchBlt(hdcInst, 0, 0, r.right - r.left, r.bottom - r.top, hdcBitmap, 0,
			0, bp.bmWidth, bp.bmHeight, SRCCOPY);

		// Cleanup

		DeleteDC(hdcBitmap);
		EndPaint(hWnd, &ps);
        }
        break;
	case WM_ERASEBKGND:
		return (LRESULT)1;
	case WM_CLOSE:

		DeleteObject(hBitmap);
		DestroyWindow(hWnd);
		break;
    case WM_DESTROY:
		DeleteObject(hBitmap);
		PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

// handler cheat injection
void DoInjection(HWND hWnd)
{
	DWORD pID = utilities::GetProcessByName("csgo.exe");

	if (pID == 0) {
		MessageBox(hWnd, "Please Start Counter Strike Global Offensive First !", "Error", MB_OK);
		return;
	}

	string CONST CSGO_CHEAT_NAME = "csgo.dll";
	TCHAR cheat_full_path[MAX_PATH];

	//get path to .dll file
	GetFullPathName(CSGO_CHEAT_NAME.c_str(), MAX_PATH, cheat_full_path, nullptr);

	bool exists = PathFileExists(cheat_full_path);

	if (!exists) {
		MessageBox(hWnd, "Injection Failed Cheat dll file not found.", "Error", NULL);
		return;
	}

	bool launched = utilities::Inject(pID, cheat_full_path);

	if (!launched) {
		MessageBox(hWnd, "Injection Failed.", "Error", NULL);
		return;
	}

	MessageBox(hWnd, "Cheat injected", "Successfull", NULL);
}
