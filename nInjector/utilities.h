#pragma once

using namespace std;

namespace utilities {

	LPCSTR ConvertString(string data);
	void copyToClipboard(HWND hWnd, const std::string &s);
	DWORD GetProcessByName(PCSTR  name);
	bool Inject(DWORD pId, char *dllName);

}