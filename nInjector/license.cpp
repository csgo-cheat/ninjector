//
// Created by A on 01/03/2018.
//
#include "stdafx.h"
#include "license.h"

using namespace base64;

namespace license {

	int getCpuHash() {
		int cpuinfo[4] = { 0, 0, 0, 0 };
		__cpuid(cpuinfo, 0);
		int hash = 0;
		auto *ptr = (int *)(&cpuinfo[0]);
		for (int i = 0; i < 8; i++)
			hash += ptr[i];

		return hash;
	}

	const char * getMachineName() {

		static char computerName[1024];
		DWORD  bufCharCount = 32767;

		GetComputerName(computerName, &bufCharCount);

		return &(computerName[0]);
	}

	int getVolumeHash() {

		DWORD serialNum = 0;

		// Determine if this volume uses an NTFS file system.
		GetVolumeInformation("c:\\", nullptr, 0, &serialNum, nullptr, nullptr, nullptr, 0);
		auto hash = (int)((serialNum + (serialNum >> 16)) & 0xFFFF);

		return hash;

	}

	std::string CreateGuid() {
		GUID pPuid{};
		CoCreateGuid(&pPuid);

		OLECHAR* guidString;
		StringFromCLSID(pPuid, &guidString);

		std::wstring ws(guidString);
		std::string str(ws.begin(), ws.end());

		return str;
	}

	std::string generateUniqueID() {

		std::string name = getMachineName();
		//std::string cpuHash = to_string(getCpuHash());
		std::string volumeHash = to_string(getVolumeHash());
		std::string guid = CreateGuid();

		//string data =  name + "." + cpuHash + "." + volumeHash + "." + guid;

		std::string data = name + "|" + volumeHash;

		return base64_encode((reinterpret_cast<const unsigned char*>(data.c_str())), data.length());

	}
}
